#!/usr/bin/env python3

import operator
from collections import OrderedDict
import numpy as np
import json

with open('stock-prices.json') as price_file:
  price_data = price_file.read()

stock_prices = json.loads(price_data)

# Create key values
keys = list(stock_prices.keys())
# Remove commas from price values so we can convert them to float data types
raw_values = list(stock_prices.values())
values = []
try:
    for stock in raw_values:
        if stock != "":
            values.append(stock.replace(',', ''))
except:
    print(f"Error with {{stock}}")

# Sort stocks by price in descending order
sorted_value_index = np.argsort(values)[::-1]
sorted_stocks = {keys[i]: float(values[i]) for i in sorted_value_index if values[i] != "null"}
sorted_stocks = sorted(sorted_stocks.items(), key=operator.itemgetter(1), reverse=True)
sorted_stocks = dict(sorted_stocks)
sorted_stocks = json.dumps(sorted_stocks, indent=2)

print(sorted_stocks)
