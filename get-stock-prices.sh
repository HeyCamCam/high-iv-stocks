#!/bin/bash

stock_ticker_list=( `jq -r '.[]' stock_tickers.json` )

for stock in "${stock_ticker_list[@]}"
do
  echo "${stock}: `curl -s https://quote.cnbc.com/quote-html-webservice/restQuote/symbolType/symbol?symbols=${stock} | jq -r ".FormattedQuoteResult.FormattedQuote[0].last"`" >> prices.json
  #sleep 2
done

jq -s -R '[[ split("\n")[] | select(length > 0) | split(": +";"") | {(.[0]): .[1]}] | add]' prices.json > prices1.json

cat prices1.json | jq ".[0]" > stock-prices.json
rm prices.json prices1.json
