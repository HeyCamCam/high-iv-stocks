#!/usr/bin/env python3

import json
import requests
from bs4 import BeautifulSoup

get_stock_webpage = requests.get("https://www.trade-ideas.com/StockInfo/high_dollar_volatility.html?more=1&symbols_only=1").content

"""Select all div elements directly inside a div element and proceeded by an h3 element.
Helpful urls
https://www.scrapingbee.com/blog/python-web-scraping-beautiful-soup/
https://stackoverflow.com/questions/46933679/scraping-text-in-h3-and-div-tags-using-beautifulsoup-python
I need to learn more about this"""

# instantiate beautifulsoup object and set parser
soup = BeautifulSoup(get_stock_webpage, 'html.parser')

# set CSS selector
selector = 'div > h3 ~ div'

# create list of stocks that will be processed
found_stocks = soup.select(selector)

# sanitize and return list of stock symbols
stock_tickers = [x.text.split(' ')[0].strip() for x in found_stocks]

# save stock tickers as a json file
with open("stock_tickers.json", "w") as stock_tickers_outfile:
   json.dump(stock_tickers, stock_tickers_outfile, indent = 2, ensure_ascii = False)
